Enterprise App
==============

A powerful Vaadin add-on to create web applications that integrate Vaadin, Hibernate, JasperReports and Quartz technologies.

License
=======

Note that enterprise-app is dual-licensed.

Enterprise App is available under the terms of the GNU Affero General Public License (AGPL) V3, and also via a special
agreement.
 
 - See license.txt for the AGPL terms.
 
 - Contact me (alejandro.d.a@gmail.com) if you are interested in using enterprise-app under a custom license.
